#!/usr/bin/env python3

import argparse
import glob
import json
from pathlib import Path

from PIL import Image
from tqdm import tqdm


def resize_polygons(polygons: dict, gt_size: tuple) -> dict:
    """
    Resize the detected polygons to the original image size.
    :param polygons: The polygons to resize.
    :param gt_size: The ground truth image size.
    :param pred_size: The prediction image size.
    :return polygons: The resized detected polygons.

    .. see: Copied from: https://gitlab.com/teklia/dla/doc-ufcn/-/blob/7ebf056432db867ec4e6b874f940afe1d6da902b/
    doc_ufcn/train/utils/evaluation.py#L20
    """
    # Compute resizing ratio
    assert "img_size" in polygons.keys()
    pred_size = tuple(polygons.get("img_size"))
    ratio = [gt / pred for gt, pred in zip(gt_size, pred_size)]

    polygons["img_size"] = gt_size

    for channel in polygons.keys():
        if channel == "img_size":
            continue
        for index, polygon in enumerate(polygons[channel]):
            x_points = [int(element[1] * ratio[0]) for element in polygon["polygon"]]
            y_points = [int(element[0] * ratio[1]) for element in polygon["polygon"]]

            x_points = [int(element) if element < gt_size[0] else int(gt_size[0]) for element in x_points]
            y_points = [int(element) if element < gt_size[1] else int(gt_size[1]) for element in y_points]
            x_points = [int(element) if element > 0 else 0 for element in x_points]
            y_points = [int(element) if element > 0 else 0 for element in y_points]

            assert max(x_points) <= gt_size[0]
            assert min(x_points) >= 0
            assert max(y_points) <= gt_size[1]
            assert min(y_points) >= 0
            polygons[channel][index]["polygon"] = list(zip(y_points, x_points))

    return polygons


def main():
    parser = argparse.ArgumentParser(
        description="Resize polygons in Doc-UFCN files so that they match the size of the original image. "
        "Generally, CNN models are trained using smaller image version to reduce the computational cost. "
        "The output, the prediction of the model is, as a consequence in this lower resolution. "
        "This program resizes the JSON files that are extracted from image masks produced by"
        "segmentation neural networks."
    )
    parser.add_argument(
        "--polygons-json",
        help="Path to the directory where are stored the polygon files to upscale (in .json format).",
        required=True,
        type=str,
    )
    parser.add_argument(
        "--original-images",
        help="Path to the directory with original images. "
        "The filenames must match the names of the .json file (expect the extension).",
    )
    parser.add_argument(
        "--output-directory", help="Where to put the reshaped .json polygon files.", required=True, type=str
    )

    args = parser.parse_args()
    polygons_json_path = args.polygons_json
    original_images_path = args.original_images
    output_directory = Path(args.output_directory)

    output_directory.mkdir(parents=True, exist_ok=True)

    polygon_files = sorted(glob.glob(f"{polygons_json_path}/*.json"))
    image_files = sorted(glob.glob(f"{original_images_path}/*.png"))
    assert len(polygon_files) == len(image_files)

    for json_idx, (json_polygon_file, image_file) in tqdm(
        enumerate(zip(polygon_files, image_files)), total=len(polygon_files)
    ):
        json_polygon_file = Path(json_polygon_file)
        image_file = Path(image_file)
        assert image_file.stem == json_polygon_file.stem

        with open(json_polygon_file, mode="r") as j_if:
            polygons = json.load(j_if)

        image = Image.open(image_file)
        new_polygons = resize_polygons(polygons, (image.height, image.width))

        with open(output_directory / json_polygon_file.name, mode="w") as j_of:
            json.dump(new_polygons, j_of, indent=2)


if __name__ == "__main__":
    main()
