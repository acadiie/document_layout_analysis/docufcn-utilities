#!/usr/bin/env python3
import argparse
import json

from pandas import DataFrame


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i",
        "--input",
        help="Path to the .json file with evaluation results obtained from run-evaluation script.",
        required=True,
        type=str,
    )
    parser.add_argument(
        "-o",
        "--output",
        help="Path to the .csv file, with content of input converted as JSON.",
        required=True,
        type=str,
    )
    parser.add_argument(
        "--no-header",
        help="Remove header from the output .csv file.",
        required=False,
        action="store_true",
        default=False,
    )

    args = parser.parse_args()

    df = DataFrame.from_dict(json.load(open(args.input, mode="r")), orient="index")
    df.index.name = "class_name"
    df.to_csv(args.output, header=not args.no_header)


if __name__ == "__main__":
    main()
