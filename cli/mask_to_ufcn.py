#!/usr/bin/env python3

import argparse
import json
from pathlib import Path
from typing import Dict, List

import cv2
import numpy
from PIL import Image


def get_polygons_in_image(image: numpy.array, category_colors: Dict[int, List[int]]):
    polygons = {"img_size": [image.shape[0], image.shape[1]]}
    for category_id, category_color in category_colors.items():
        contours, _ = cv2.findContours(
            numpy.all(image == category_color, axis=2).astype(numpy.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE
        )
        polygons[category_id] = []
        if len(contours) > 1:
            for contour in contours:
                coordinates = contour.squeeze().tolist()
                if len(coordinates) >= 4:  # a polygon has at least 4 points
                    polygon = {"confidence": 1, "polygon": contour.squeeze().tolist()}
                    polygons[category_id].append(polygon)
    return polygons


def main():
    parser = argparse.ArgumentParser(
        description="Convert a mask of a segmentation prediction into a JSON file that can be used in order to "
        "identify the segmentation and compute evaluation metrics. It takes as input an extra file that"
        "describes the different classes, represented by different colors in the segmentation masks."
    )
    parser.add_argument(
        "--mask-path", type=str, help="Path to the image mask that contains the segmentations.", required=True
    )
    parser.add_argument(
        "--json-output-path",
        type=str,
        help="Name of the file where segmentations will be exported to. "
        "By default, will be exported in the image folder, with .json extension.",
        required=False,
    )
    parser.add_argument(
        "--categories-colors",
        type=str,
        help="Path to the JSON file that contains RGB information about the colors in the masks. "
        "Each object must contain an id, a name and color, that is a list of ints representing the RGB color.",
        required=True,
    )
    args = parser.parse_args()
    mask_path = Path(args.mask_path)
    categories_colors = json.load(open(Path(args.categories_colors), mode="r"))

    # Replace the suffix of the mask if no json_output_path parameter is given
    output_path = mask_path.with_suffix(".json")
    if args.json_output_path:
        output_path = Path(args.json_output_path)
    output_path.parent.mkdir(parents=True, exist_ok=True)

    mask_as_numpy = numpy.array(Image.open(mask_path).convert("RGB"))
    segmentation_as_doc_ufcn_format = get_polygons_in_image(
        mask_as_numpy, {x["id"]: x["color"] for x in categories_colors}
    )

    with open(output_path, "w") as o_jf:
        json.dump(segmentation_as_doc_ufcn_format, o_jf, indent=2)


if __name__ == "__main__":
    main()
