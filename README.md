# `doc-ufcn` utilities.

In this repository, we provide the tools to manipulate the predictions made by a segmentation model that outputs segmentation masks. The segmentation are all merged into one file as described in the image below. Here, each colors matches a different class: green is for text line, blue for page header, orange for page footer and red for table headers. This is the segmentation mask of a census table image.

![An example of a segmentation, with multiple colors.](docs/segmentation_example.png)

The provides provides two utilities:

- `convert-mask-to-docufcn-polygon`: this is used to convert a simple image segmentation mask to a `.json` file that describes the segmentation, as detailed in [Data and configuration](#data-and-configuration).
- `resize-docufcn-polygons`: with this tool, you can reshape the segmentation of a `.json` file. It is used in the context where you trained and tested the neural network on small images but would like to upscale the resolution in order to match the original image size.

## Installation

## Data and configuration

The JSON files that contain the segmentations have the following structure:

```json
{
  "img_size": [
    height,
    width
  ],
  "class1": [
    {
      "confidence": confidence,
      "polygon": [...]
    }
  ],
  "class2": [
    {
      "confidence": confidence,
      "polygon": [...]
    },
    {
      "confidence": confidence,
      "polygon": [...]
    }
  ]
}
```

And the JSON file used to identify each segmentation class from the image color has the following structure:

```json
[
  {
    "id": 1,
    "name": "text_line",
    "color": [
      0,
      153,
      0
    ]
  }
]
```

## Command line interface

